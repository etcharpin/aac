#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

string manacherAlgorithm(const string& s) {
    string modifiedString = "#";
    for (char c : s) {
        modifiedString += c;
        modifiedString += '#';
    }

    int n = modifiedString.length();
    vector<int> palindromeLength(n, 0);

    int center = 0, right = 0;
    int maxLen = 0, maxCenter = 0;

    for (int i = 0; i < n; i++) {
        int mirror = 2 * center - i;

        if (i < right) {
            palindromeLength[i] = min(right - i, palindromeLength[mirror]);
        }

        int a = i + (1 + palindromeLength[i]);
        int b = i - (1 + palindromeLength[i]);

        while (a < n && b >= 0 && modifiedString[a] == modifiedString[b]) {
            palindromeLength[i]++;
            a++;
            b--;
        }

        if (i + palindromeLength[i] > right) {
            center = i;
            right = i + palindromeLength[i];
        }

        if (palindromeLength[i] > maxLen) {
            maxLen = palindromeLength[i];
            maxCenter = i;
        }
    }

    int start = (maxCenter - maxLen) / 2;
    return s.substr(start, maxLen);
}

int main() {
    string inputString;
    cout << "Entrez la chaine de caractères : ";
    cin >> inputString;

    string longestPalindrome = manacherAlgorithm(inputString);

    cout << "Le plus long palindrome est : " << longestPalindrome << endl;

    return 0;
}
