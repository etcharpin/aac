#include <stdio.h>

#define max(a, b) ((a > b) ? a : b)

int sac_a_dos_multidimensionnel(int W_max, int V_max, int poids[], int valeur[], int volume[], int N, char *noms[]) {
    int dp[N + 1][W_max + 1][V_max + 1];

    // Initialisation
    for (int i = 0; i <= N; i++) {
        for (int j = 0; j <= W_max; j++) {
            for (int k = 0; k <= V_max; k++) {
                if (i == 0 || j == 0 || k == 0) {
                    dp[i][j][k] = 0;
                } else {
                    dp[i][j][k] = -1;
                }
            }
        }
    }

    // Remplissage de la matrice
    for (int i = 1; i <= N; i++) {
        for (int j = 0; j <= W_max; j++) {
            for (int k = 0; k <= V_max; k++) {
                if (poids[i - 1] <= j && volume[i - 1] <= k) {
                    dp[i][j][k] = max(dp[i - 1][j][k], dp[i - 1][j - poids[i - 1]][k - volume[i - 1]] + valeur[i - 1]);
                } else {
                    dp[i][j][k] = dp[i - 1][j][k];
                }
            }
        }
    }

    // Récupération des objets inclus
    int i = N, j = W_max, k = V_max;
    printf("Objets inclus : ");
    while (i > 0 && j > 0 && k > 0) {
        if (dp[i][j][k] != dp[i - 1][j][k]) {
            printf("%s ", noms[i - 1]);
            j -= poids[i - 1];
            k -= volume[i - 1];
        }
        i--;
    }
    printf("\n");

    return dp[N][W_max][V_max];
}

int main() {
    int W_max = 20;
    int V_max = 18;
    int poids[] = {2, 5, 3, 4, 1, 6, 2, 3, 4};
    int valeur[] = {7, 12, 9, 8, 5, 15, 6, 10, 11};
    int volume[] = {3, 7, 5, 6, 1, 9, 4, 2, 8};
    char *noms[] = {"rubis", "diamants", "saphirs", "émeraudes", "or", "perles fines", "topazes", "améthystes", "pierres précieuses mixtes"};
    int N = sizeof(poids) / sizeof(poids[0]);

    int resultat = sac_a_dos_multidimensionnel(W_max, V_max, poids, valeur, volume, N, noms);

    printf("Valeur maximale obtenue : %d\n", resultat);

    return 0;
}
